# Mini blog test

# Getting Started with Create a new repository and Clone the project into local

`git clone https://gitlab.com/radhoinbhk/mini-blog-test.git
cd mini-blog-test
`

## Install all the npm packages.

Go into the project directory, and type the following command to install all npm packages:

`npm install
`

## if you need to change the URL of the server back-end.

go to config file into path 'mini-blog-test\src\config\config.js'

## In order to run the application Type the following command

`npm run start`

The Application Runs on localhost:3000

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

