///////////////////////////////////////////////////////////////////
//                                                               //
//          EndPoint                                     //
//                                                               //
///////////////////////////////////////////////////////////////////
import {server} from "./config";

const endpoint = {
  POST_AUTH_LOGIN: `${server}api/login`,
  POST_AUTH_REGISTER: `${server}api/register`,
  GET_ALL_POST: `${server}api/getPost`,
  ADD_POST: `${server}api/addPost`,
  ADD_COMMENT: `${server}api/addComment`,
  DELET_POST: `${server}api/deletPost`,
  DELET_COMMENT: `${server}api/deletComment`,
}

export {endpoint}
