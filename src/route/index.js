import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Signin from '../page/signin/index'
import Signup from '../page/signup/index'
import Blog from '../page/blog'
import NotFound from '../component/NotFound'
import { useSelector } from "react-redux";

export default function AuthExample() {

  return (
    <Router>
      <Switch>
        <PublicRoute path="/signup">
          <Signup />
        </PublicRoute>
        <PrivateRoute path="/blog">
          <Blog />
        </PrivateRoute>
        <PublicRoute path="/">
          <Signin />
        </PublicRoute>
        <Route path="*">
          <NotFound />
        </Route>
      </Switch>
    </Router>
  );
}

function PublicRoute({ children, ...rest }) {
  const userData = useSelector(state => state.userLogged.userData)

  return (
    <Route
      {...rest}
      render={({ location }) =>
        userData.token ? (
          <Redirect
            to={{
              pathname: "/blog",
              state: { from: location }
            }}
          />
        ) : (
            children
          )
      }
    />
  );
}

/** 
 * A wrapper for <Route> that redirects to the login
 * screen if you're not yet authenticated.
*/
function PrivateRoute({ children, ...rest }) {
  const userData = useSelector(state => state.userLogged.userData)
  return (
    <Route
      {...rest}
      render={({ location }) =>
        userData.token ? (
          children
        ) : (
            <Redirect
              to={{
                pathname: "/",
                state: { from: location }
              }}
            />
          )
      }
    />
  );
}