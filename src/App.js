import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import RouterApp from './route/index'
import './App.css'
import { useStore } from '../src/redux/configStore'
import { BrowserRouter, Redirect, useLocation } from 'react-router-dom';

function App(props) {
  const { store, persistor } = useStore(props.initialReduxState)
  // const location = useLocation();

  // useEffect(() => {
  //   if (location.pathname == '/') {
  //     <Redirect
  //       to={{
  //         pathname: "/dashboard",
  //       }}
  //     />
  //   }
  // }, [])


  return (
    <>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <BrowserRouter>
            <RouterApp />
          </BrowserRouter>
        </PersistGate>
      </Provider>
    </>
  );
}

export default App;
