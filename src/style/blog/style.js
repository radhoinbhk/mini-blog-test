import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  containerBlog: {
    padding: '50px 0',
  },
  logoutButton: {
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-end',
    marginBottom: 50,
  },
  contentPost: {
    padding: 20,
    marginBottom: 40
  },
  postField: {
    width: '100%'
  },
  containerButton: {
    marginTop: 20,
    display: 'flex',
    justifyContent: 'flex-end',
  },
  dateText: {
    color: '#969696'
  },
  contentPersonnel: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: 20
  },
  avatarPersonnel: {
    width: 50,
    height: 50,
    marginRight: 10
  },
  newComment: {
    marginTop: 40
  },
  containerComment: {
    display: 'flex',
    justifyContent: 'space-between',
    backgroundColor: '#F5F5F5',
    padding: 10,
    marginTop: 20,
    borderRadius: 10,
  },
  contentComentTxt: {
    display: 'flex',
    alignItems: 'center'
  },
  commentName: {
    marginRight: 5,
    fontWeight: 600
  },
  buttonProgress: {
    color: "#3f51b5"
  },
}));
