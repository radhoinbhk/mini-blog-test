import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  containerSignin: {
    height: "100vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    padding: "0 50px",
    [theme.breakpoints.down('sm')]: {
      padding: 0
    }
  },
  buttonProgress: {
    color: "#3f51b5"
  },
  containerMessage: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center"
  },
  avatar: {
    margin: 10,
    backgroundColor: theme.palette.secondary.main,
  },
  titel: {
    fontWeight: "bold"
  },
  titelSignup: {
    marginBottom: 50
  },
  textField: {
    width: "100%",
    marginBottom: 20
  },
  containerFieldsm: {
    width: "100%",
    marginBottom: 20,
    display: "flex",
    justifyContent: "space-between"
  },
  textFieldSm: {
    width: "49%"
  },
  button: {
    width: "100%",
    marginBottom: 20
  },
  messageTxt: {
    width: "max-content",
    margin: 0
  },
  messageLink: {
    marginLeft: 10,
    cursor: "pointer",
    textDecoration: "none",
    [theme.breakpoints.down("xs")]: {
    marginLeft: 10,
    margin: 0
    },
  }
}));
