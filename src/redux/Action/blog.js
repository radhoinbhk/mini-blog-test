import { POST_LOADING, ALL_POST, ADD_POST_ERROR, ADD_POST_SUCCESS, ADD_POST_LOADING, ADD_COMMENT_LOADING, ADD_COMMENT_SUCCESS, ADD_COMMENT_ERROR } from "./actionTypes";
import { endpoint } from '../../config/Api'
import { httpClient } from "../interceptors";


export const postIsLoading = (bool) => {
    return {
        type: POST_LOADING,
        postIsLoading: bool,
    };
};

export const setAllPost = (data) => {
    return {
        type: ALL_POST,
        allPost: data,
    };
};

export const getAllPost = () => {
    return (dispatch) => {
        dispatch(postIsLoading(true))
        httpClient.get(endpoint.GET_ALL_POST)
            .then((res) => {
                if (res.status == 200) {
                    console.log("res", res.data);
                    dispatch(setAllPost(res.data))
                    dispatch(postIsLoading(false))
                }
            })
            .catch((err) => {
                console.log("err", err);
                dispatch(postIsLoading(false))
            });
    };
};

export const addComment = (data) => {
    console.log("data", data);
    return (dispatch) => {
        dispatch(addCommentIsLoading(data.post_id))
        httpClient
            .post(endpoint.ADD_COMMENT, data)
            .then((res) => {
                console.log("res", res.data);
                if (res.status == 200) {
                    dispatch(getAllPost())
                    dispatch(addCommentSuccess(true))
                    dispatch(addCommentIsLoading(""))
                }
            })
            .catch((err) => {
                console.log("err", err);
                dispatch(addCommentError(true))
                dispatch(addCommentIsLoading(""))
            });
    };
};

export const addCommentIsLoading = (bool) => {
    return {
        type: ADD_COMMENT_LOADING,
        addCommentIsLoading: bool,
    };
};

export const addCommentSuccess = (bool) => {
    return {
        type: ADD_COMMENT_SUCCESS,
        addCommentSuccess: bool,
    };
};

export const addCommentError = (bool) => {
    return {
        type: ADD_COMMENT_ERROR,
        addCommentError: bool,
    };
};

export const deletComment = (idComment) => {
    return (dispatch) => {
        httpClient
            .delete(`${endpoint.DELET_COMMENT}/${idComment}`)
            .then((res) => {
                if (res.status == 200) {
                    dispatch(setAllPost(res.data))
                }
            })
            .catch((err) => {
                console.log("err", err);
            });
    };
};

export const addPost = (data) => {
    return (dispatch) => {
        dispatch(addPostIsLoading(true))
        httpClient
            .post(endpoint.ADD_POST, data)
            .then((res) => {
                if (res.status == 200) {
                    dispatch(getAllPost())
                    dispatch(addPostSuccess(true))
                    dispatch(addPostIsLoading(false))
                }
            })
            .catch((err) => {
                console.log("err", err);
                dispatch(addPostError(true))
                dispatch(addPostIsLoading(false))
            });
    };
};

export const deletPost = (idPost) => {
    return (dispatch) => {
        httpClient
            .delete(`${endpoint.DELET_POST}/${idPost}`)
            .then((res) => {
                if (res.status == 200) {
                    console.log("ree", res.data);
                    dispatch(setAllPost(res.data))
                }
            })
            .catch((err) => {
                console.log("err", err);
            });
    };
};

export const addPostIsLoading = (bool) => {
    return {
        type: ADD_POST_LOADING,
        addPostIsLoading: bool,
    };
};

export const addPostSuccess = (bool) => {
    return {
        type: ADD_POST_SUCCESS,
        addPostSuccess: bool,
    };
};

export const addPostError = (bool) => {
    return {
        type: ADD_POST_ERROR,
        addPostError: bool,
    };
};
