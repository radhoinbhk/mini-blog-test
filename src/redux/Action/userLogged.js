import { USER_DATA } from "./actionTypes";

export const setUserData = (data) => {
    return {
        type: USER_DATA,
        userData: data,
    };
};
