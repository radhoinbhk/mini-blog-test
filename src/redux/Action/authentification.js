import { REGISTER_LOADING, SIGNUP_ERROR, SIGNUP_SUCCESS, LOGIN_LOADING, LOGIN_ERROR, USER_DATA } from "./actionTypes";
import axios from "axios";
// import * as constants from '../../config/constant'
import { setUserData } from "./userLogged";
import { endpoint } from '../../config/Api'

export const userLoginIsLoading = (bool) => {
    return {
        type: LOGIN_LOADING,
        loginIsLoading: bool,
    };
};

export const userLoginError = (bool) => {
    return {
        type: LOGIN_ERROR,
        loginError: bool,
    };
};

export const login = (data) => {
    return (dispatch) => {
        dispatch(userLoginIsLoading(true))
        axios.post(endpoint.POST_AUTH_LOGIN, data)
            .then((res) => {
                if (res.status == 200) {
                    console.log("res", res.data);
                    dispatch(setUserData({
                        id: res.data.user.id,
                        name: res.data.user.name,
                        mail: res.data.user.email,
                        token: res.data.token
                    }));
                    dispatch(userLoginIsLoading(false))
                }
            })
            .catch((err) => {
                console.log("err", err);
                dispatch(userLoginIsLoading(false))
                dispatch(userLoginError(true))
            });
    };
};

export const register = (data) => {
    return (dispatch) => {
        dispatch(userRegisterIsLoading(true))
        axios
            .post(endpoint.POST_AUTH_REGISTER, data)
            .then((res) => {
                if (res.status == 200) {
                    dispatch(userRegisterSuccess(true))
                    dispatch(userRegisterIsLoading(false))
                }
            })
            .catch((err) => {
                console.log("err", err);
                dispatch(userRegisterError(true))
                dispatch(userRegisterIsLoading(false))
            });
    };
};

export const userRegisterIsLoading = (bool) => {
    return {
        type: REGISTER_LOADING,
        registerIsLoading: bool,
    };
};

export const userRegisterSuccess = (bool) => {
    return {
        type: SIGNUP_SUCCESS,
        registerSuccess: bool,
    };
};

export const userRegisterError = (value) => {
    return {
        type: SIGNUP_ERROR,
        registerError: value,
    };
};
