/**
 * authentification
 */
export const SIGNUP_ERROR = "SIGNUP_ERROR";
export const SIGNUP_SUCCESS = "SIGNUP_SUCCESS";
export const REGISTER_LOADING = "REGISTER_LOADING";
export const LOGIN_LOADING = "LOGIN_LOADING";
export const LOGIN_ERROR = "LOGIN_ERROR";
export const USER_DATA = "USER_DATA";

/**
 * blog
 */
export const POST_LOADING = "POST_LOADING";
export const ALL_POST = "ALL_POST";
export const ADD_POST_LOADING = "ADD_POST_LOADING";
export const ADD_POST_SUCCESS = "ADD_POST_SUCCESS";
export const ADD_POST_ERROR = "ADD_POST_ERROR";
export const ADD_COMMENT_LOADING = "ADD_COMMENT_LOADING";
export const ADD_COMMENT_SUCCESS = "ADD_COMMENT_SUCCESS";
export const ADD_COMMENT_ERROR = "ADD_COMMENT_ERROR";