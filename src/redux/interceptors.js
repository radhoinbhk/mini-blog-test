import axios from 'axios';

// Add a request interceptor
axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    const userLogged = JSON.parse(localStorage.getItem('persist:root')).userLogged
    var jwtToken = ""
    if (userLogged) {
        jwtToken = JSON.parse(userLogged).userData.token
    }
    if (jwtToken && jwtToken != "") {
        config.headers = { Authorization: "Bearer " + jwtToken }
    }
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// Add a response interceptor
axios.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
}, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
});

export const httpClient = axios