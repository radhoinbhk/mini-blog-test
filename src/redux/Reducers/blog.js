import { ADD_COMMENT_ERROR, ADD_COMMENT_LOADING, ADD_COMMENT_SUCCESS, ADD_POST_ERROR, ADD_POST_LOADING, ADD_POST_SUCCESS, ALL_POST, POST_LOADING } from "../Action/actionTypes";

const initialState = {
  postIsLoading: false,
  addPostIsLoading: false,
  addPostSuccess: false,
  addPostError: false,
  addCommentIsLoading: "",
  addCommentSuccess: false,
  addCommentError: false,
  allPost: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_LOADING:
      return {
        ...state,
        postIsLoading: action.postIsLoading,
      };
    case ALL_POST:
      return {
        ...state,
        allPost: action.allPost,
      };
    case ADD_POST_LOADING:
      return {
        ...state,
        addPostIsLoading: action.addPostIsLoading,
      };
    case ADD_POST_SUCCESS:
      return {
        ...state,
        addPostSuccess: action.addPostSuccess,
      };
    case ADD_POST_ERROR:
      return {
        ...state,
        addPostError: action.addPostError,
      };
    case ADD_COMMENT_LOADING:
      return {
        ...state,
        addCommentIsLoading: action.addCommentIsLoading,
      };
    case ADD_COMMENT_SUCCESS:
      return {
        ...state,
        addCommentSuccess: action.addCommentSuccess,
      };
    case ADD_COMMENT_ERROR:
      return {
        ...state,
        addCommentError: action.addCommentError,
      };
    default:
      return state;
  }
};
export default reducer;
