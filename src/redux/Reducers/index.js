import { combineReducers } from "redux";
import userLogged from "./userLogged";
import authentification from "./authentification";
import blog from "./blog";

// COUNTER REDUCER

// COMBINED REDUCERS
const reducers = {
    blog,
    userLogged,
    authentification
};

export default combineReducers(reducers);
