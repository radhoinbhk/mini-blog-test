import { USER_DATA } from "../Action/actionTypes";

const initialState = {
  userData: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_DATA:
      return {
        ...state,
        userData: action.userData,
      };
    default:
      return state;
  }
};
export default reducer;
