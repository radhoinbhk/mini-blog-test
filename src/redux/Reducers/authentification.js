import { SIGNUP_ERROR, SIGNUP_SUCCESS, REGISTER_LOADING, LOGIN_LOADING, LOGIN_ERROR, USER_DATA } from "../Action/actionTypes";

const initialState = {
  registerError: false,
  registerSuccess: false,
  registerIsLoading: false,
  loginIsLoading: false,
  loginError: false,
  userData: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SIGNUP_ERROR:
      return {
        ...state,
        registerError: action.registerError,
      };
    case SIGNUP_SUCCESS:
      return {
        ...state,
        registerSuccess: action.registerSuccess,
      };
    case REGISTER_LOADING:
      return {
        ...state,
        registerIsLoading: action.registerIsLoading,
      };
    case LOGIN_LOADING:
      return {
        ...state,
        loginIsLoading: action.loginIsLoading,
      };
    case LOGIN_ERROR:
      return {
        ...state,
        loginError: action.loginError,
      };
    case USER_DATA:
      return {
        ...state,
        userData: action.userData,
      };
    default:
      return state;
  }
};
export default reducer;
