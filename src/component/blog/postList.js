import React, { useEffect, useState } from "react";
import { Avatar, Button, CircularProgress, IconButton, Paper, Snackbar, TextField, Typography } from '@material-ui/core';
import moment from 'moment';
import DeleteIcon from '@material-ui/icons/Delete';
import MuiAlert from '@material-ui/lab/Alert';
import { addComment, addCommentError, addCommentSuccess, deletPost, deletComment } from "../../redux/Action/blog";
import { useStyles } from "../../style/blog/style";
import { useDispatch, useSelector } from "react-redux";

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const PostList = (props) => {
    const classes = useStyles()
    const dispatch = useDispatch()
    const userData = useSelector(state => state.userLogged.userData)
    const addCommentIsLoading = useSelector(state => state.blog.addCommentIsLoading)
    const addCommentSucc = useSelector(state => state.blog.addCommentSuccess)
    const addCommentErr = useSelector(state => state.blog.addCommentError)
    const [commenField, setCommenField] = useState("")
    const [snackbarError, setSnackbarError] = useState(false)

    useEffect(() => {
        if (addCommentSucc) {
            setCommenField("")
            dispatch(addCommentSuccess(false))
        }
    }, [addCommentSucc])

    useEffect(() => {
        if (addCommentErr) {
            changeErrSnackbar(true)
            dispatch(addCommentError(false))
        }
    }, [addCommentErr])

    const changeErrSnackbar = (value) => {
        setSnackbarError(value)
    };

    const removePost = (idPost) => {
        dispatch(deletPost(idPost))
    }

    const removeComment = (idComment) => {
        dispatch(deletComment(idComment))
    }

    const saveComment = (postId) => {
        const data = {
            user_id: userData.id,
            post_id: postId,
            comment: commenField,
            created_date: moment().tz('Africa/Tunis').format('YYYY/MM/DD HH:mm:s'),
        }
        dispatch(addComment(data))
    }

    return (
        <Paper className={classes.contentPost} key={props.post.id}>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <div className={classes.contentPersonnel}>
                    <Avatar className={classes.avatarPersonnel} />
                    <div>
                        <Typography variant="h6">{props.post.user.name}</Typography>
                        <Typography variant="subtitle1" className={classes.dateText}>
                            {moment(props.post.created_at).tz('Africa/Tunis').format("DD/MM/YYYY HH:mm:ss")}
                        </Typography>
                    </div>
                </div>
                {userData.id == props.post.user.id && <IconButton color="secondary" style={{ padding: 20 }} onClick={() => removePost(props.post.id)}>
                    <DeleteIcon />
                </IconButton>}
            </div>
            <Typography variant="body1">{props.post.statut}</Typography>
            {props.post.comments.map((data) =>
                <div className={classes.containerComment} key={data.id}>
                    <div>
                        <div className={classes.contentComentTxt}>
                            <Typography variant="subtitle1" className={classes.commentName}>{data.user.name}:</Typography>
                            <Typography variant="body2">{data.comment}</Typography>
                        </div>
                        <Typography variant="subtitle1" className={classes.dateText}>
                            {moment(data.created_at).tz('Africa/Tunis').format("DD/MM/YYYY HH:mm:ss")}
                        </Typography>
                    </div>
                    {userData.id == data.user.id && <IconButton color="secondary" style={{ padding: 20 }} onClick={() => removeComment(data.id)}>
                        <DeleteIcon />
                    </IconButton>}
                </div>
            )}
            <div className={classes.newComment}>
                <TextField
                    className={classes.postField}
                    id="outlined-multiline-static"
                    label="Enter your comment"
                    size="small"
                    onChange={(e) => setCommenField(e.target.value)}
                    value={commenField}
                    multiline
                    variant="outlined"
                />
                <div className={classes.containerButton}>
                    <Button variant="contained" color="primary" disabled={addCommentIsLoading == props.post.id} key={props.post.id} onClick={() => saveComment(props.post.id)}>
                        {addCommentIsLoading == props.post.id ? <CircularProgress size={24} className={classes.buttonProgress} />
                            : "Publish"}
                    </Button>
                </div>
            </div>
            <Snackbar open={snackbarError} autoHideDuration={6000} onClose={() => changeErrSnackbar(false)}>
                <Alert onClose={() => changeErrSnackbar(false)} severity="error">
                    add comment failed
                </Alert>
            </Snackbar>
        </Paper>
    );
}

export default PostList;
