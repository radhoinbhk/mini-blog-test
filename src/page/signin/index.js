import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Controller, useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import { Avatar, Button, Container, FormControl, FormHelperText, IconButton, InputAdornment, InputLabel, OutlinedInput, Snackbar, TextField, Typography } from "@material-ui/core";
import { Visibility, Send, VisibilityOff, LockOutlined } from "@material-ui/icons";
import MuiAlert from '@material-ui/lab/Alert';
import { useStyles } from "../../style/signin/style";
import { login, userLoginError } from "../../redux/Action/authentification";


function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const Signin = (props) => {
    const { handleSubmit, errors, control } = useForm();
    const dispatch = useDispatch()
    const classes = useStyles()
    const [values, setValues] = useState({
        username: "",
        password: "",
    });
    const [showPassword, setShowPassword] = useState(false)
    const [snackbarError, setSnackbarError] = useState(false)
    const loginError = useSelector(state => state.authentification.loginError)

    useEffect(() => {
        if (loginError) {
            changeErrSnackbar(true)
            dispatch(userLoginError(false))
        }
    }, [loginError])

    const changeErrSnackbar = (value) => {
        setSnackbarError(value)
    };

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };
    const onSubmit = (value) => {
        const data = {
            email: value.email,
            password: value.password,
        };
        console.log("data", data);
        dispatch(login(data))
    };

    return (
        <Container maxWidth="sm">
            <div className={classes.containerSignin}>
                <Avatar className={classes.avatar}>
                    <LockOutlined />
                </Avatar>
                <Typography className={classes.titel} color="primary" variant="h4" gutterBottom>
                    Se connecter
                </Typography>
                <div className={classes.containerMessage} style={{ marginBottom: 20 }}>
                    <Typography className={classes.messageTxt} style={{ color: "#546e7a" }} variant="h6" gutterBottom>
                        Vous n'avez pas de compte ?
                    </Typography>
                    <Link to="/signup" className={classes.messageLink}>
                        <Typography className={classes.messageTxt} variant="h6" gutterBottom>
                            S'inscrire.
                        </Typography>
                    </Link>
                </div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Controller
                        variant="outlined"
                        control={control}
                        as={<TextField required={true} helperText={errors.email && 'invalid email address'} />}
                        className={classes.textField}
                        label="Adresse e-mail"
                        name="email"
                        defaultValue=""
                        error={errors.email}
                        rules={{
                            pattern: {
                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i
                            }
                        }}
                    />
                    <FormControl required={true} error={errors.password} className={classes.textField} variant="outlined">
                        <InputLabel htmlFor="outlined-adornment-password">Mot de passe</InputLabel>
                        <Controller
                            variant="outlined"
                            control={control}
                            as={<OutlinedInput
                                required
                                id="outlined-adornment-password"
                                type={showPassword ? 'text' : 'password'}
                                value={values.password}
                                onChange={handleChange("password")}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                        >
                                            {showPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                labelWidth={110}
                            />}
                            name="password"
                            rules={{
                                minLength: 8
                            }}
                        />
                        {errors.password && <FormHelperText error={true}>invalid Mot de passe</FormHelperText>}
                    </FormControl>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        className={classes.button}
                        endIcon={<Send />}
                    >
                        Se connecter
                    </Button>
                </form>
                <div className={classes.containerMessage}>
                    <Typography className={classes.messageTxt} variant="subtitle1" gutterBottom>
                        Mot de passe oublié ?
                    </Typography>
                    <Link className={classes.messageLink} to="/" style={{ lineHeight: "28px" }}>Réinitialiser le mot de passe</Link>
                </div>
            </div>
            <Snackbar open={snackbarError} autoHideDuration={6000} onClose={() => changeErrSnackbar(false)}>
                <Alert onClose={() => changeErrSnackbar(false)} severity="error">
                    login failed
                </Alert>
            </Snackbar>
        </Container>
    );
};
export default Signin;
