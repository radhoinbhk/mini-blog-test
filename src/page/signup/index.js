import React, { useEffect, useState } from "react";
import { Avatar, Button, CircularProgress, Container, Snackbar, TextField, Typography } from "@material-ui/core";
import { useDispatch, useSelector } from 'react-redux';
import { Controller, useForm } from "react-hook-form";
import { useStyles } from "../../style/signin/style";
import { LockOutlined, Send } from "@material-ui/icons";
import MuiAlert from '@material-ui/lab/Alert';
import { Link, useHistory } from "react-router-dom";
import { register, userRegisterError, userRegisterSuccess } from "../../redux/Action/authentification";

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const SignUp = (props) => {
    const { handleSubmit, errors, control } = useForm();
    const dispatch = useDispatch()
    const history = useHistory();
    const registerIsLoading = useSelector((state) => state.authentification.registerIsLoading);
    const registerSuccess = useSelector((state) => state.authentification.registerSuccess);
    const registerErr = useSelector((state) => state.authentification.registerError);

    const classes = useStyles()
    const [snackbarError, setSnackbarError] = useState(false)
    const [values, setValues] = useState({
        name: "",
        email: "",
        password: "",
        Confpassword: "",
    });

    useEffect(() => {
        if (registerErr) {
            changeErrSnackbar(true)
            dispatch(userRegisterError(false))
        }
    }, [registerErr])

    useEffect(() => {
        if (registerSuccess) {
            dispatch(userRegisterSuccess(false))
            history.push("/")
        }
    }, [registerSuccess])

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const changeErrSnackbar = (value) => {
        setSnackbarError(value)
    };

    const onSubmit = handleSubmit((value) => {
        const data = {
            name: values.name,
            email: value.email,
            password: values.password
        }
        dispatch(register(data))
    })

    return (
        <Container maxWidth="sm">
            <div className={classes.containerSignin}>
                <Avatar className={classes.avatar}>
                    <LockOutlined />
                </Avatar>
                <Typography className={[classes.titel, classes.titelSignup]} color="primary" variant="h4" gutterBottom>
                    S'inscrire
                </Typography>
                <form onSubmit={onSubmit}>
                    <TextField required={true} className={classes.textField} value={values.name} name="name" onChange={handleChange("name")} id="outlined-basic" label="Nom" variant="outlined" />
                    <Controller
                        variant="outlined"
                        control={control}
                        as={<TextField required={true} helperText={errors.email && errors.email.message} />}
                        className={classes.textField}
                        label="Adresse e-mail"
                        name="email"
                        defaultValue=""
                        error={errors.email}
                        rules={{
                            pattern: {
                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                message: "invalid email address"
                            }
                        }}
                    />
                    <TextField
                        id='filled-password-input'
                        label='Mot de passe'
                        name="password"
                        type='password'
                        autoComplete='current-password'
                        variant='outlined'
                        required
                        error={errors.Confpassword}
                        value={values.password}
                        onChange={handleChange('password')}
                        className={classes.textField}
                        helperText={errors.Confpassword && "les mots de passe ne correspondent pas"}
                    />
                    <Controller
                        variant="outlined"
                        control={control}
                        className={classes.textField}
                        label="Confirmer le mot de passe"
                        name="Confpassword"
                        defaultValue=""
                        error={errors.Confpassword}
                        rules={{ validate: (value) => value === values.password }}
                        as={<TextField
                            value={values.Confpassword}
                            type="password"
                            onChange={handleChange("Confpassword")}
                            helperText={errors.Confpassword && "les mots de passe ne correspondent pas"}
                        />}
                    />
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        className={classes.button}
                        disabled={registerIsLoading}
                        endIcon={!registerIsLoading && <Send />}
                    >
                        {registerIsLoading ? <CircularProgress size={24} className={classes.buttonProgress} />
                            : "S'inscrire"}
                    </Button>
                </form>
                <Typography className={classes.messageTxt} variant="subtitle1" gutterBottom>
                    Already have an account ?
                    <Link style={{ marginLeft: 10, textDecoration: "none" }} to="/">Se connecter</Link>
                </Typography>
            </div>
            <Snackbar open={snackbarError} autoHideDuration={6000} onClose={() => changeErrSnackbar(false)}>
                <Alert onClose={() => changeErrSnackbar(false)} severity="error">
                    informations incorrect
                </Alert>
            </Snackbar>
        </Container>
    );
};
export default SignUp;
