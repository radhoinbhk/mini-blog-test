import React, { useEffect, useState } from "react";
import { Button, CircularProgress, Container, Paper, Snackbar, TextField, Typography } from '@material-ui/core';
import { useDispatch, useSelector } from "react-redux";
import MuiAlert from '@material-ui/lab/Alert';
import moment from 'moment-timezone';
import { useStyles } from "../../style/blog/style";
import { addPost, addPostError, addPostSuccess, getAllPost } from "../../redux/Action/blog";
import PostList from "../../component/blog/postList";
import { ExitToApp } from '@material-ui/icons';
import { setUserData } from "../../redux/Action/userLogged";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const Blog = (props) => {
  const classes = useStyles()
  const dispatch = useDispatch()
  const allPost = useSelector(state => state.blog.allPost)
  const addPostIsLoading = useSelector(state => state.blog.addPostIsLoading)
  const addPostSucc = useSelector(state => state.blog.addPostSuccess)
  const addPostErr = useSelector(state => state.blog.addPostError)
  const userData = useSelector(state => state.userLogged.userData)
  const [postField, setPostField] = useState("")
  const [snackbarError, setSnackbarError] = useState(false)

  useEffect(() => {
    dispatch(getAllPost())
  }, [])

  useEffect(() => {
    if (addPostSucc) {
      setPostField("")
      dispatch(addPostSuccess(false))
    }
  }, [addPostSucc])

  useEffect(() => {
    if (addPostErr) {
      console.log("addPostErr", addPostErr);
      changeErrSnackbar(true)
      dispatch(addPostError(false))
    }
  }, [addPostErr])

  const changeErrSnackbar = (value) => {
    setSnackbarError(value)
  };

  const savePost = () => {
    const data = {
      user_id: userData.id,
      statut: postField,
      created_date: moment().tz('Africa/Tunis').format('YYYY/MM/DD HH:mm:s')
    }
    dispatch(addPost(data))
  }

  const logout = () => {
    dispatch(setUserData({}))
    localStorage.clear()
  }
  return (
    <Container maxWidth="sm" className={classes.containerBlog}>
      <div className={classes.logoutButton}>
        <Button variant="contained" color="primary" endIcon={<ExitToApp />} onClick={() => logout()}>
          Logout
      </Button>
      </div>
      <Paper className={classes.contentPost}>
        <Typography variant="h6" gutterBottom>
          hello user, what's new ?
        </Typography>
        <TextField
          className={classes.postField}
          id="outlined-multiline-static"
          label="Enter your post"
          onChange={(e) => setPostField(e.target.value)}
          value={postField}
          multiline
          rows={4}
          variant="outlined"
        />
        <div className={classes.containerButton}>
          <Button variant="contained" color="primary" disabled={addPostIsLoading} onClick={() => savePost()}>
            {addPostIsLoading ? <CircularProgress size={24} className={classes.buttonProgress} />
              : "Publish"}
          </Button>
        </div>
      </Paper>
      {allPost.map((post) => <PostList post={post} key={post.id} />)}
      <Snackbar open={snackbarError} autoHideDuration={6000} onClose={() => changeErrSnackbar(false)}>
        <Alert onClose={() => changeErrSnackbar(false)} severity="error">
          add post failed
        </Alert>
      </Snackbar>
    </Container>
  );
}

export default Blog;
